from django.shortcuts import render, redirect
from todos.models import TodoItem, TodoList
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.
def show_todo_list(request):
    todo_list_list = TodoList.objects.all()
    context = {"todo_list_list": todo_list_list}
    return render(request, "todos/list.html", context)


def show_todo_list_detail(request, id):
    todo_list_detail = TodoList.objects.get(id=id)
    items = TodoItem.objects.filter(list=todo_list_detail)
    context = {"todo_list_detail": todo_list_detail, "items": items}
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list_instance = form.save()
            return redirect("todo_list_detail", id=todo_list_instance.id)
    else:
        form = TodoListForm()

    context = {"form": form}

    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    todo_list_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list_instance)
        if form.is_valid():
            todo_list_instance = form.save()
            return redirect("todo_list_detail", id=todo_list_instance.id)
    else:
        form = TodoListForm(instance=todo_list_instance)

    context = {"form": form}

    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    todo_list_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_list_instance.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()

    context = {"form": form}

    return render(request, "items/create.html", context)


def todo_item_update(request, id):
    todo_item_instance = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item_instance)
        if form.is_valid():
            todo_item_instance = form.save()
            return redirect("todo_list_detail", id=todo_item_instance.list.id)
    else:
        form = TodoItemForm(instance=todo_item_instance)

    context = {"form": form}

    return render(request, "items/edit.html", context)
